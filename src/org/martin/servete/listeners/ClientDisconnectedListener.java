/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.martin.servete.listeners;

import org.martin.servete.net.Client;

/**
 *
 * @author martin
 */
public interface ClientDisconnectedListener {
    public void onClientDisconnected(Client client);
}
