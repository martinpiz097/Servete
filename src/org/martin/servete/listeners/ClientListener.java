/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.martin.servete.listeners;

//import java.util.EventListener;

/**
 *
 * @author martin
 */
public interface ClientListener {
    public boolean isClientConnected();
    public void onClientInExecution();
}
