/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.martin.servete.net;

import java.io.IOException;
import java.net.Socket;
import java.util.LinkedList;
import java.util.List;

import org.martin.servete.exceptions.NullListenersException;
import org.martin.servete.exceptions.ServeteException;
import org.martin.servete.io.interfaces.Communicable;
import org.martin.servete.io.streams.ServeteInputStream;
import org.martin.servete.io.streams.ServeteOutputStream;
import org.martin.servete.listeners.ClientListener;

/**
 *
 * @author martin
 */
public class Client extends Thread implements Communicable {
    private final Socket sockClient;
    private final ServeteOutputStream output;
    private final ServeteInputStream input;
    private ClientListener ownListener;
    private List listCliObjects; // Lista para manejar objetos como un objeto
                                // usuario o lo que el programador necesite.

//    public enum STREAMS_TYPE{
//        OBJECTS, DATA, GENERIC;
//        // String, File, etc
//    }
    
    // Depende del tipo de streams a instanciar es como trabajara el cliente
    // y asi permite poder interactuar de diferentes maneras en los listeners
//    public Client(Socket sockClient, STREAMS_TYPE streamsType) throws IOException {
//        this(sockClient, new ObjectOutputStream(sockClient.getOutputStream()), 
//                new ObjectInputStream(sockClient.getInputStream()));
//    }
    public Client(Socket sockClient) throws IOException {
        this(sockClient, new ServeteOutputStream(sockClient.getOutputStream()),
                new ServeteInputStream(sockClient.getInputStream()));
    }
    
    public Client(Socket sockClient, ServeteOutputStream output,
                  ServeteInputStream input) {
        this.sockClient = sockClient;
        this.output = output;
        this.input = input;
        listCliObjects = new LinkedList();
        setName("Client "+getId());
    }
    
    public void addObjectToClientList(Object obj){
        listCliObjects.add(obj);
    }

    public ClientListener getOwnListener() {
        return ownListener;
    }

    public void setOwnListener(ClientListener ownListener) {
        this.ownListener = ownListener;
    }
    
    public List getListCliObjects() {
        return listCliObjects;
    }

    public void setListCliObjects(List listCliObjects) {
        this.listCliObjects = listCliObjects;
    }
    
    @Override
    public void send(byte[] data) throws IOException {
        output.writeBytes(data);
    }

    @Override
    public void send(Object obj) throws IOException {
        output.writeObject(obj);
    }

    @Override
    public void send(String str) throws IOException {
        output.writeString(str);
    }

    @Override
    public void send(int data) throws IOException {
        output.writeInt(data);
    }

    @Override
    public void send(byte data) throws IOException {
        output.writeByte(data);
    }

    @Override
    public byte[] receiv() throws IOException {
        return input.readBytes();
    }

    @Override
    public Object receivObject() throws IOException, ClassNotFoundException {
        return input.readObject();
    }

    @Override
    public String receivMsg() throws IOException {
        return input.readString();
    }

    @Override
    public int receivInt() throws IOException {
        return input.readInt();
    }

    @Override
    public byte receivByte() throws IOException {
        return input.readByte();
    }

    @Override
    public void run() {
        if (ownListener == null)
            throw new NullListenersException("Client listener is null");
        else
            while (ownListener.isClientConnected())
            ownListener.onClientInExecution();
    }

}
