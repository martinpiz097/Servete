package org.martin.servete.exceptions;

public class ServeteException extends Exception {
    public ServeteException() {
    }

    public ServeteException(String message) {
        super(message);
    }

    public ServeteException(String message, Throwable cause) {
        super(message, cause);
    }

    public ServeteException(Throwable cause) {
        super(cause);
    }

    public ServeteException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }

}
