package org.martin.servete.exceptions;

public class ServeteRuntimeException extends RuntimeException {
    public ServeteRuntimeException() {
    }

    public ServeteRuntimeException(String message) {
        super(message);
    }

    public ServeteRuntimeException(String message, Throwable cause) {
        super(message, cause);
    }

    public ServeteRuntimeException(Throwable cause) {
        super(cause);
    }

    public ServeteRuntimeException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }

}
