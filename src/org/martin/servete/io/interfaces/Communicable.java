package org.martin.servete.io.interfaces;

public interface Communicable extends Transmissible, Receivable {}
