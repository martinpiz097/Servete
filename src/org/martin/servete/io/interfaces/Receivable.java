package org.martin.servete.io.interfaces;

import java.io.IOException;

public interface Receivable {
    public byte[] receiv() throws IOException;
    public Object receivObject() throws IOException, ClassNotFoundException;
    public String receivMsg() throws IOException;
    public int receivInt() throws IOException;
    public byte receivByte() throws IOException;
}
