package org.martin.servete.io.interfaces;

import java.io.IOException;
import java.io.Serializable;

public interface Transmissible {
    public void send(byte[] data) throws IOException;
    public void send(Object obj) throws IOException;
    public void send(String str) throws IOException;
    public void send(int data) throws IOException;
    public void send(byte data) throws IOException;
}
