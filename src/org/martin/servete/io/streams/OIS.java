package org.martin.servete.io.streams;

import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.StreamCorruptedException;

public class OIS extends ObjectInputStream {
    public OIS(InputStream in) throws IOException {
        super(in);
    }

    protected OIS() throws IOException, SecurityException {
    }

    @Override
    protected void readStreamHeader() throws IOException, StreamCorruptedException {}

}
