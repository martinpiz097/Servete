/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.martin.servete.io.streams;

import java.io.*;

/**
 *
 * @author martin
 */
public class ServeteOutputStream extends DataOutputStream {

    public ServeteOutputStream(OutputStream socketStream) {
        super(socketStream);
    }

    private void writeData(final byte[] data) throws IOException {
        int dataLen = data.length;
        System.err.println("WriteDataAvailable: "+dataLen);
        writeInt(dataLen);
        write(data, 0, dataLen);
    }

    public void writeString(final String str) throws IOException {
        writeData(str.getBytes());
    }

    public void writeBytes(final byte[] data) throws IOException {
        writeData(data);
    }

    public void writeObject(Object obj) throws IOException {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        OOS oos = new OOS(baos);
        oos.writeObject(obj);
        oos.close();
        writeData(baos.toByteArray());
        baos.close();
    }

    /*
    public static void main(String[] args) {
        int test = 64;
        // >>> ó >> --> dividir por dos las veces que el numero lo defina
        // << --> multiplicar por dos
        System.out.println(test<<1);
        System.out.println(0xFF);
        System.out.println(-1 & 0xFF);
        int i = 1000;
        System.out.println("--------------------");
        System.out.println(i>>>24);
        System.out.println(i>>>16);
        System.out.println(i>>>8);
        System.out.println(i>>>0);
        System.out.println("--------------------");
        int i1 = i>>>24 & 255;
        int i2 = i>>>16 & 255;
        int i3 = i>>>8 & 255;
        int i4 = i>>>0 & 255;
        System.out.println(i1);
        System.out.println(i2);
        System.out.println(i3);
        System.out.println(i4);
        System.out.println("--------------------");
        i1 = i1 << 24;
        i2 = i2 << 16;
        i3 = i3 << 8;
        i4 = i4 << 0;
        System.out.println(i1);
        System.out.println(i2);
        System.out.println(i3);
        System.out.println(i4);
        System.out.println(i1+i2+i3+i4);
    }

    */


}
