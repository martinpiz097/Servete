package org.martin.servete.io.streams;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.OutputStream;

public class OOS extends ObjectOutputStream {
    public OOS(OutputStream out) throws IOException {
        super(out);
    }

    protected OOS() throws IOException, SecurityException {
    }

    @Override
    protected void writeStreamHeader() throws IOException {}

}
