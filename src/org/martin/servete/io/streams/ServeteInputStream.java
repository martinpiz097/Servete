/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.martin.servete.io.streams;

import java.io.*;


/**
 *
 * @author martin
 */
public class ServeteInputStream extends DataInputStream {

    public ServeteInputStream(InputStream socketStream) {
        super(socketStream);
    }

    private void waitForData() throws IOException {
        while (available() == 0){
            try {
                Thread.sleep(1);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    private byte[] readData() throws IOException {
        waitForData();
        System.err.println("Available: "+available());
        int available = readInt();
        System.err.println("ReadDataAvailable: "+available);
        byte[] buffer = new byte[available];
        for (int i = 0; i < available; i++)
            buffer[i] = (byte) read();
        return buffer;
    }

    public String readString() throws IOException {
        return new String(readData());
    }

    public byte[] readBytes() throws IOException {
        return readData();
    }

    public Object readObject() throws IOException, ClassNotFoundException {
        ByteArrayInputStream bais = new ByteArrayInputStream(readData());
        OIS ois = new OIS(bais);
        Object obj = ois.readObject();
        ois.close();
        bais.close();
        return obj;
    }

}
